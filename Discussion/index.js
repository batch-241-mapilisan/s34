// Use the "require" directive to load the express module/package
const { response } = require('express');
const express = require('express')

// Create an express application
const app = express()

// For our application server to run, we need a port to listen to
const port = 4000;

// Allow your app to read json data
app.use(express.json());

// Allow your app to read other data types
// {extended: true} - by applying option, it allows us to receive information in other data types
app.use(express.urlencoded({extended: true}))


// [SECTION] Routes
// Express has methods corresponding to each HTTP method

// GET
// This route expects to receive a GET request at URI "/greet"
app.get('/greet', (req, res) => {
    res.send("Hello from the /greet endpoint")
})

// POST
app.post('/hello', (req, res) => {
    res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
})

let users = [];

// Simple registration form
app.post('/signup', (req, res) => {
    if (req.body.username !== '' && req.body.password !== '') {
        users.push(req.body)
        res.send(`User ${req.body.username} successfully registered`)
    } else {
        res.send("Please input BOTH username and password.")
    }
})

// Change password
app.patch('/change-password', (req, res) => {
    let message;

    for (let i = 0; i < users.length; i++) {
        if (req.body.username === users[i].username) {
            users[i].password = req.body.password
            message = `User ${req.body.username}'s password has been updated`
            break;
        } else {
            message = "User does not exist"
        }
    }

    res.send(message)
})






app.listen(port, () => console.log(`Server running at port ${port}`));