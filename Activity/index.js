const express = require('express')

const app = express()

const port = 4000;

app.use(express.json())
app.use(express.urlencoded({extended: true}))

let users = []

// GET
// Homepage
app.get('/home', (req, res) => {
    res.send('Welcome to the home page')
})

// GET
// All users
app.get('/users', (req, res) => {
    res.send(users)
})

// POST
// Create user
app.post('/signup', (req, res) => {
    const { username, password } = req.body

    if (!username || !password) {
        res.status(400).send('Please input both username and password')
    } else {
        users.push({
            username,
            password
        })
        res.status(200).send(`Username ${username} has successfully registered`)
    }
})

// PATCH
// Change password
app.patch('/change-password', (req, res) => {
    const {username, password} = req.body

    if (!username) {
        res.status(400).send('Please input your username')
    } else if (!password) {
        res.status(400).send('Please input a valid password. Password must not be empty')
    } 
    
    let message;
    
    for (let i = 0; i < users.length; i++) {
        if (users[i].username === username) {
            users[i].password = password
            message = `User ${username}'s password has been successfully updated`
            break;
        } else {
            message = 'User does not exist'
        }
    }
    res.send(message)
})

// DELETE
// Delete User
app.delete('/delete-user', (req, res) => {
    const { username } = req.body

    let message;

    for (let i = 0; i < users.length; i++) {
        if (users[i].username === username) {
            message = `User ${username} has been successfully deleted`
            break;       
        } else (
            message = 'User does not exist'
        )
    }
    users = users.filter(user => user.username !== username)
    res.send(message)
})

app.listen(port, () => console.log(`Server is up and running on port ${port}`))